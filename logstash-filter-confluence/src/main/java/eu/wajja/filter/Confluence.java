package eu.wajja.filter;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.Base64;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.io.IOUtils;
import org.jsoup.Jsoup;
import org.jsoup.safety.Safelist;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.elastic.logstash.api.Configuration;
import co.elastic.logstash.api.Context;
import co.elastic.logstash.api.Event;
import co.elastic.logstash.api.Filter;
import co.elastic.logstash.api.FilterMatchListener;
import co.elastic.logstash.api.LogstashPlugin;
import co.elastic.logstash.api.PluginConfigSpec;

/**
 * Simple tool to fetch http content and send it to logstash
 * 
 * @author mahytom
 *
 */
@LogstashPlugin(name = "confluence")
public class Confluence implements Filter {

	private static final Logger LOGGER = LoggerFactory.getLogger(Confluence.class);

	private static final String PROPERTY_METADATA_CUSTOM = "metadataCustom";
	private static final String PROPERTY_SIMPLIFIED_CONTENT_TYPE = "simplifiedContentType";

	private String threadId;
	private ConfluenceTikaServerService tikaServerService;
	private Map<String, Object> metadataCustom;
	private Map<String, Object> simplifiedContentType;

	private static final PluginConfigSpec<Map<String, Object>> CONFIG_METADATA_CUSTOM = PluginConfigSpec.hashSetting(PROPERTY_METADATA_CUSTOM, new HashMap<>(), false, false);
	private static final PluginConfigSpec<Map<String, Object>> CONFIG_SIMPLIFIED_CONTENT_TYPE = PluginConfigSpec.hashSetting(PROPERTY_SIMPLIFIED_CONTENT_TYPE, new HashMap<>(), false, false);
	public static final PluginConfigSpec<String> CONFIG_TIKA_SERVER = PluginConfigSpec.stringSetting("ticketServerEndpoint", "http://tika-server.tools:9998");
	
	/** metadata fields arriving from the confluence fetcher **/

	private static final String METADATA_TITLE = "title";
	private static final String METADATA_REFERENCE = "reference";
	private static final String METADATA_CONTENT = "content";
	private static final String METADATA_URL = "url";
	private static final String METADATA_CONTENT_TYPE = "contentType";
	private static final String METADATA_MODIFIED_DATE = "modifiedDate";
	private static final String METADATA_MODIFIED_USER = "modifiedBy";
	private static final String METADATA_SPACE_ID = "spaceId";
	private static final String METADATA_SPACE_NAME = "spaceName";
	private static final String METADATA_SPACE_URL = "spaceUrl";
	private static final String METADATA_PARENT_NAME = "parentName";
	private static final String METADATA_PARENT_URL = "parentUrl";

	/** metadata fields that need to be generated **/

	private static final String METADATA_MAP_AUTHOR = "AUTHOR";
	private static final String METADATA_MAP_CONFLUENCESPACE = "CONFLUENCESPACE";
	private static final String METADATA_MAP_DATAORIGIN = "DATAORIGIN";
	private static final String METADATA_MAP_DATE = "DATE";
	private static final String METADATA_MAP_DOCUMENT_TYPE = "DOCUMENT_TYPE";
	private static final String METADATA_MAP_PAGENAME = "PAGENAME";
	private static final String METADATA_MAP_PAGEURI = "PAGEURI";
	private static final String METADATA_MAP_SIMPLIFIED_CONTENT_TYPE = "SIMPLIFIED_CONTENT_TYPE";
	private static final String METADATA_MAP_SPACEURI = "SPACEURI";
	private static final String METADATA_MAP_SPACEHOME = "SPACEHOME";
	private static final String METADATA_MAP_TITLE = "TITLE";
	private static final String METADATA_MAP_CONTENT_TYPE = "CONTENT-TYPE";

	/**
	 * Mandatory constructor
	 * 
	 * @param id
	 * @param config
	 * @param context
	 * @throws IOException
	 */
	public Confluence(String id, Configuration config, Context context) {

		if (context != null && LOGGER.isDebugEnabled()) {
			LOGGER.debug(context.toString());
		}

		this.threadId = id;
		this.metadataCustom = config.get(CONFIG_METADATA_CUSTOM);
		this.simplifiedContentType = config.get(CONFIG_SIMPLIFIED_CONTENT_TYPE);
		this.tikaServerService = new ConfluenceTikaServerService(config.get(CONFIG_TIKA_SERVER));
	}

	/**
	 * Returns a list of all configuration
	 */
	@Override
	public Collection<PluginConfigSpec<?>> configSchema() {
		return Arrays.asList(CONFIG_METADATA_CUSTOM, CONFIG_SIMPLIFIED_CONTENT_TYPE, CONFIG_TIKA_SERVER);
	}

	@Override
	public String getId() {
		return this.threadId;
	}

	@Override
	public Collection<Event> filter(Collection<Event> events, FilterMatchListener filterMatchListener) {

		events.stream().forEach(event -> {

			Map<String, Object> eventData = event.getData();

			if (eventData.containsKey(METADATA_URL) && eventData.containsKey(METADATA_CONTENT) && eventData.get(METADATA_CONTENT_TYPE).toString().startsWith("confluence")) {

				String url = eventData.get(METADATA_URL).toString();
				String confluenceContentType = eventData.get(METADATA_CONTENT_TYPE).toString();

				LOGGER.info("Parsing Confluence filter METADATA_URL -> {}", url);

				eventData.put(METADATA_MAP_AUTHOR, eventData.get(METADATA_MODIFIED_USER).toString());
				eventData.put(METADATA_MAP_CONFLUENCESPACE, eventData.get(METADATA_SPACE_NAME).toString());
				eventData.put(METADATA_MAP_DATAORIGIN, eventData.get(METADATA_SPACE_ID).toString());
				eventData.put(METADATA_MAP_DATE, eventData.get(METADATA_MODIFIED_DATE).toString());

				if (eventData.containsKey(METADATA_PARENT_NAME)) {
					eventData.put(METADATA_MAP_PAGENAME, eventData.get(METADATA_PARENT_NAME).toString());
				}else {
					eventData.put(METADATA_MAP_PAGENAME, eventData.get(METADATA_REFERENCE).toString());
				}

				if (eventData.containsKey(METADATA_PARENT_URL)) {
					eventData.put(METADATA_MAP_PAGEURI, eventData.get(METADATA_PARENT_URL).toString());
				}else {
					eventData.put(METADATA_MAP_PAGEURI, eventData.get(METADATA_URL).toString());
				}

				eventData.put(METADATA_MAP_SPACEURI, eventData.get(METADATA_SPACE_URL).toString());
				eventData.put(METADATA_MAP_SPACEHOME, eventData.get(METADATA_SPACE_NAME).toString());
				eventData.put(METADATA_MAP_TITLE, eventData.get(METADATA_TITLE).toString());
				eventData.put(METADATA_MAP_DOCUMENT_TYPE, eventData.get(METADATA_CONTENT_TYPE).toString());

				/**
				 * Detects content type
				 */

				String contentString = eventData.get(METADATA_CONTENT).toString();
				byte[] bytes = Base64.getDecoder().decode(contentString);
				
				String contentType = tikaServerService.detectMimetype(bytes);
				eventData.put(METADATA_MAP_CONTENT_TYPE, contentType);

				/**
				 * Clean up HTML
				 */

				if (confluenceContentType.equals("confluence/page")) {

					try {
						
						String content = IOUtils.toString(bytes, StandardCharsets.UTF_8.name());
						content = Jsoup.clean(content, Safelist.none());
						eventData.put(METADATA_CONTENT, Base64.getEncoder().encodeToString(content.getBytes()));

					} catch (IOException e) {
						LOGGER.error("Failed to parse content", e);
					}

				}

				/**
				 * Add the configured metadata
				 */

				metadataCustom.entrySet().stream().forEach(entry -> eventData.put(entry.getKey(), entry.getValue()));

				/**
				 * Simplified Content Type
				 */
		
                if (contentType != null) {

                    String simpleContentType = contentType.substring(contentType.indexOf("/") + 1);

                    if (this.simplifiedContentType.containsKey(simpleContentType)) {
                        eventData.put(METADATA_MAP_SIMPLIFIED_CONTENT_TYPE, simplifiedContentType.get(simpleContentType));
                    } else {
                        LOGGER.info("Could not map simplified content type from url : {}, detected simple : {}", url, simpleContentType);
                    }

                }

			}

		});

		return events;

	}
}
