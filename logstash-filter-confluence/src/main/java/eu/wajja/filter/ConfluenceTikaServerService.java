package eu.wajja.filter;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;

import org.apache.commons.io.IOUtils;

public class ConfluenceTikaServerService {

    private String tikaServerEndpoint;

    public ConfluenceTikaServerService(String tikaServerEndpoint) {

        this.tikaServerEndpoint = tikaServerEndpoint;
    }

    public String detectMimetype(byte[] bytes) {

        if (bytes == null || bytes.length == 0) {
            return null;
        }

        try {
            URL url = new URL(tikaServerEndpoint + "/detect/stream");

            HttpURLConnection httpCon = (HttpURLConnection) url.openConnection();
            httpCon.setDoOutput(true);
            httpCon.setRequestMethod("PUT");

            addToOutputStream(httpCon, bytes);
            return getJson(httpCon);

        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;

    }

    private void addToOutputStream(HttpURLConnection httpURLConnection, byte[] bytes) {

        try (DataOutputStream outputStream = new DataOutputStream(httpURLConnection.getOutputStream())) {
            outputStream.write(bytes);

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private String getJson(HttpURLConnection httpURLConnection) {

        try (InputStream inputStream = httpURLConnection.getInputStream()) {
            return IOUtils.toString(httpURLConnection.getInputStream(), StandardCharsets.UTF_8);

        } catch (IOException e) {
            e.printStackTrace();
        }

        return "";

    }
}
