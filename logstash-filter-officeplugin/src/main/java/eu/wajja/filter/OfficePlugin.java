package eu.wajja.filter;

import java.io.IOException;
import java.util.Arrays;
import java.util.Base64;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.jruby.RubyString;
import org.jsoup.Jsoup;
import org.jsoup.safety.Safelist;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.elastic.logstash.api.Configuration;
import co.elastic.logstash.api.Context;
import co.elastic.logstash.api.Event;
import co.elastic.logstash.api.Filter;
import co.elastic.logstash.api.FilterMatchListener;
import co.elastic.logstash.api.LogstashPlugin;
import co.elastic.logstash.api.PluginConfigSpec;

/**
 * Simple tool to fetch http content and send it to logstash
 * 
 * @author mahytom
 *
 */
@LogstashPlugin(name = "officeplugin")
public class OfficePlugin implements Filter {

    private static final Logger LOGGER = LoggerFactory.getLogger(OfficePlugin.class);

    private OfficeTikaServerService tikaServerService;

    private static final List<String> LANGUAGES = Arrays.asList("en",
            "et",
            "ro",
            "sq",
            "tr",
            "mt",
            "el",
            "fr",
            "lt",
            "lv",
            "hu",
            "is",
            "no",
            "ca",
            "bs",
            "hr",
            "sl",
            "sv",
            "fi",
            "sr",
            "ru",
            "eu",
            "cy",
            "cs",
            "bg",
            "it",
            "nl",
            "ga",
            "be",
            "uk",
            "es",
            "da",
            "de",
            "pl",
            "pt",
            "sk",
            "lb",
            "mk",
            "ar",
            "zh");

    protected static final String PROPERTY_METADATA = "metadata";
    protected static final String PROPERTY_METADATA_CUSTOM = "metadataCustom";

    private static final PluginConfigSpec<Map<String, Object>> CONFIG_METADATA_CUSTOM = PluginConfigSpec.hashSetting(PROPERTY_METADATA_CUSTOM, new HashMap<>(), false, false);
    public static final PluginConfigSpec<String> CONFIG_TIKA_SERVER = PluginConfigSpec.stringSetting("ticketServerEndpoint", "http://tika-server.tools:9998");

    private static final String METADATA_TITLE = "TITLE";
    private static final String METADATA_DATE = "DATE";
    private static final String METADATA_CONTENT_TYPE = "CONTENT-TYPE";
    private static final String METADATA_URL = "url";
    private static final String METADATA_CONTENT = "content";
    private static final String METADATA_TYPE = "type";
    private static final String METADATA_REFERENCE = "reference";
    private static final String METADATA_LANGUAGES = "languages";
    private static final String METADATA_CONTENT_DISPOSITION = "Content-Disposition";

    private String threadId;
    private Map<String, Object> metadataCustom;

    /**
     * Mandatory constructor
     * 
     * @param id
     * @param config
     * @param context
     * @throws IOException
     */
    public OfficePlugin(String id, Configuration config, Context context) {

        if (context != null && LOGGER.isDebugEnabled()) {
            LOGGER.debug(context.toString());
        }

        this.threadId = id;
        this.tikaServerService = new OfficeTikaServerService(config.get(CONFIG_TIKA_SERVER));
        this.metadataCustom = config.get(CONFIG_METADATA_CUSTOM);
    }

    /**
     * Returns a list of all configuration
     */
    @Override
    public Collection<PluginConfigSpec<?>> configSchema() {

        return Arrays.asList(CONFIG_METADATA_CUSTOM, CONFIG_TIKA_SERVER);
    }

    @Override
    public String getId() {

        return this.threadId;
    }

    @Override
    public Collection<Event> filter(Collection<Event> events, FilterMatchListener filterMatchListener) {

        events.stream().forEach(event -> {

            Map<String, Object> eventData = event.getData();

            if (eventData.containsKey(METADATA_URL) && eventData.containsKey(METADATA_CONTENT)) {

                String contentString = eventData.get(METADATA_CONTENT).toString();
                byte[] bytes = Base64.getDecoder().decode(contentString);

                /**
                 * Detects type if does not exist
                 */

                Map<String, Object>[] tikaResponse = tikaServerService.extractContentAndMetadata(bytes);
                String type = null;
                String content = null;
                Map<String, String> metadata = new HashMap<>();

                if (tikaResponse != null && tikaResponse.length > 0) {

                    Map<String, Object> map = tikaResponse[0];

                    for (Entry<String, Object> entry : map.entrySet()) {

                        if (entry.getValue() instanceof String) {

                            if (entry.getKey().equals("X-TIKA:content")) {
                                content = (String) entry.getValue();

                            } else if (entry.getKey().equals("Content-Type")) {
                                type = (String) entry.getValue();

                            } else if (!entry.getKey().contains("X-TIKA")) {
                                metadata.put(entry.getKey(), (String) entry.getValue());
                            }

                        }
                    }

                }

                if (eventData.containsKey(METADATA_TYPE)) {
                    type = eventData.get(METADATA_TYPE).toString();

                } else {
                    eventData.put(METADATA_TYPE, type);
                }

                // Only parse Office Docs here

                if (type.contains("office") || type.contains("msword")) {

                    String reference = ((RubyString) eventData.get(METADATA_REFERENCE)).toString();
                    LOGGER.info("Found document with type {}, {}", type, reference);

                    try {

                        /**
                         * Detects Title of Document
                         */

                        String title = metadata.keySet().stream().filter(name -> name.contains("title")).map(name -> metadata.get(name).trim()).filter(value -> !value.isEmpty()).findFirst().orElse(null);

                        if (title != null && !title.isEmpty()) {
                            eventData.put(METADATA_TITLE, title);
                        } else {

                            if (eventData.containsKey(METADATA_CONTENT_DISPOSITION) && eventData.get(METADATA_CONTENT_DISPOSITION).toString().contains("filename")) {

                                String filename = eventData.get(METADATA_CONTENT_DISPOSITION).toString();
                                filename = filename.substring(filename.indexOf("filename")).replace("filename", "").trim();

                                if (filename.startsWith("=")) {
                                    filename = filename.substring(1);
                                }

                                if (filename.contains(".")) {
                                    filename = filename.substring(0, filename.lastIndexOf('.'));
                                }

                                filename = filename.replaceAll("[^A-Za-z0-9]", " ").trim();

                                eventData.put(METADATA_TITLE, filename);

                            } else {

                                String url = eventData.get(METADATA_URL).toString();
                                url = url.substring(url.lastIndexOf('/') + 1);

                                if (url.contains(".")) {
                                    url = url.substring(0, url.lastIndexOf('.'));
                                }

                                eventData.put(METADATA_TITLE, url);

                            }

                        }

                        /**
                         * Detects Modified Date
                         */

                        String date = metadata.keySet().stream().filter(name -> name.contains("modified")).map(name -> metadata.get(name).trim()).filter(value -> !value.isEmpty()).findFirst().orElse(null);

                        if (date != null) {
                            eventData.put(METADATA_DATE, date);
                        }

                        /**
                         * Detects the language if language is not specified
                         */
                        if (!eventData.containsKey(METADATA_LANGUAGES)) {

                            content = Jsoup.clean(content, Safelist.none());                            
                            String language = tikaServerService.detectLanguage(content);

                            if (LANGUAGES.contains(language)) {
                                eventData.put(METADATA_LANGUAGES, Arrays.asList(language));
                            }
                        }

                        /**
                         * Add Metadata Field
                         */

                        if (!metadataCustom.isEmpty()) {
                            eventData.put(PROPERTY_METADATA, metadataCustom);
                        }

                    } catch (Exception e) {
                        LOGGER.error("Failed to extract Office Document", e);
                    }

                }

                eventData.put(METADATA_CONTENT_TYPE, type);
            }

        });

        return events;

    }

}
