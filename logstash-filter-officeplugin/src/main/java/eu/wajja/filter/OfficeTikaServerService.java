package eu.wajja.filter;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.Map;

import org.apache.commons.io.IOUtils;

import com.fasterxml.jackson.databind.ObjectMapper;

public class OfficeTikaServerService {

    private String tikaServerEndpoint;
    private ObjectMapper objectMapper = new ObjectMapper();

    public OfficeTikaServerService(String tikaServerEndpoint) {

        this.tikaServerEndpoint = tikaServerEndpoint;
    }

    public Map<String, Object>[] extractContentAndMetadata(byte[] bytes) {

        if (bytes == null || bytes.length == 0) {
            return null;
        }

        try {
            URL url = new URL(tikaServerEndpoint + "/rmeta");

            HttpURLConnection httpCon = (HttpURLConnection) url.openConnection();
            httpCon.setRequestProperty("maxEmbeddedResources", "0");
            httpCon.setDoOutput(true);
            httpCon.setRequestMethod("PUT");

            addToOutputStream(httpCon, bytes);
            String json = getJson(httpCon);

            return objectMapper.readValue(json, Map[].class);

        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    public String detectLanguage(String content) {

        if (content == null) {
            return null;
        }

        try {
            URL url = new URL(tikaServerEndpoint + "/language/stream");

            HttpURLConnection httpCon = (HttpURLConnection) url.openConnection();
            httpCon.setDoOutput(true);
            httpCon.setRequestMethod("PUT");

            addToOutputStream(httpCon, content);
            return getJson(httpCon);

        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    private void addToOutputStream(HttpURLConnection httpURLConnection, String content) {

        try (OutputStreamWriter outputStream = new OutputStreamWriter(httpURLConnection.getOutputStream())) {
            outputStream.write(content);

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void addToOutputStream(HttpURLConnection httpURLConnection, byte[] bytes) {

        try (DataOutputStream outputStream = new DataOutputStream(httpURLConnection.getOutputStream())) {
            outputStream.write(bytes);

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private String getJson(HttpURLConnection httpURLConnection) {

        try (InputStream inputStream = httpURLConnection.getInputStream()) {
            return IOUtils.toString(httpURLConnection.getInputStream(), StandardCharsets.UTF_8);

        } catch (IOException e) {
            e.printStackTrace();
        }

        return "";

    }

}
